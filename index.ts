export interface MediaFile {
  /**
   * The full path of the file, including the name.
   */
  fullPath?: string;
  /**
   * The full path of the thumbnail file, including the name.
   */
  fullThumbnailPath?: string;
  /**
   * The date and time when the file was last modified.
   */
  lastModifiedDate?: number;
  /**
   * the cdvfile url of the thumbnail file
   */
  localThumbnailURL?:string;
  /**
   * the cdvfile url of the file
   */
  localURL?:string;
  /**
   * The name of the file, without path information.
   */
  name?: string;
    /**
   * The size of the file, in bytes.
   */
  size?: number;
  /**
   * The name of the thumbnail file, without path information.
   */
  thumbnailName?: string;
  /**
   * The file's mime type
   */
  type?: string;
}

export interface CaptureVideoOptions {
    /**
   * Maximum duration per video clip in seconds.
   */
  duration: number;
  /**
   * the directory the video should be saved in.
   */
  outputDir: string;
  /**
   * Maximum number of video clips to record. This value is ignored on iOS, only one video clip can be taken per invocation.
   */
  //limit?: number;
  /**
   * Quality of the video. This parameter can only be used with Android.
   */
  //quality?: number;
}

export interface AfCamera {
  /**
   * Start the video recorder application and return information about captured video clip files.
   * @param options
   * @param onSuccess the success callback returns a MediaFile object
   * @param onError this failure callback returns the error from the plugin
   */
  captureVideo(options: CaptureVideoOptions, onSuccess?:( mediaFile:MediaFile) => void, onError?:(error:Error) => void): void;

}

interface AfCameraPlugin {
  afcamera?:AfCamera;
}

declare global {
  interface Window { 
    plugins: AfCameraPlugin;
  }
}