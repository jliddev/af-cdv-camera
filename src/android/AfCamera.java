package biz.appform.camera;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.UUID;

import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.file.FileUtils;
import org.apache.cordova.file.LocalFilesystemURL;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import static android.app.Activity.RESULT_OK;

public class AfCamera extends CordovaPlugin {
    private static final String TAG = AfCamera.class.getSimpleName();
    private static final String ACTION_RECORD_VIDEO = "recordVideo";

    private static final int RECORD_REQ = 203;

    private final long mDefaultDuration = 240000;
    private CallbackContext mCallbackContext;

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
    }

    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        JSONObject options = args.optJSONObject(0);

        if (ACTION_RECORD_VIDEO.equals(action)) {
            mCallbackContext = callbackContext;
            this.captureVideo(options);
        } else {
            return false;
        }

        return true;
    }

    private void captureVideo(JSONObject options) {
        String outputDir = options.optString("outputDir", null);
        long duration = options.optLong("duration", -1);

        if (outputDir == null || outputDir.isEmpty()) {
            mCallbackContext.error("outputDir option cannot be null or empty");
            return;
        }

        File outputDirFile = new File(Uri.parse(outputDir).getPath());
        if (!outputDirFile.exists()) {
            if (!outputDirFile.mkdirs()) {
                Log.e(TAG, "Failed to create outputdir: " + outputDir);
            }
        }
        File saveFile = new File(outputDirFile, UUID.randomUUID().toString() + ".mp4");

        Intent afIntent = new Intent(cordova.getActivity(), AFCameraActivity.class);
        if (duration > 1000) {
            afIntent.putExtra("duration", (int) duration);
        } else {
            afIntent.putExtra("duration", (int) mDefaultDuration);
        }

        afIntent.putExtra("filepath", saveFile.getAbsolutePath());

//        VideoRecorderActivity.IntentBuilder builder = new VideoRecorderActivity.IntentBuilder(cordova.getActivity())
//                .quality(AbstractCameraActivity.Quality.LOW)
//                .to(saveFile)
//                .chronoType(ChronoType.COUNT_DOWN)
//                .requestPermissions();
//
//        if( duration > 1000){
//            builder.durationLimit((int)duration);
//        } else {
//            builder.durationLimit((int)mDefaultDuration);
//        }
//
//        Intent i = builder.build();

        cordova.startActivityForResult(this, afIntent, RECORD_REQ);
    }


    /**
     * Called when the video view exits.
     *
     * @param requestCode The request code originally supplied to startActivityForResult(),
     *                    allowing you to identify who this result came from.
     * @param resultCode  The integer result code returned by the child activity through its setResult().
     * @param intent      An Intent, which can return result data to the caller (various data can be attached to Intent "extras").
     * @throws JSONException
     */
    public void onActivityResult(int requestCode, int resultCode, final Intent intent) {
        if (requestCode == RECORD_REQ) {
            if (resultCode == RESULT_OK) {
                Uri fileUri = intent.getData();
                createVideoResult(fileUri);
            } else {
                mCallbackContext.error("User canceled recording.");
            }
        }
    }

    private void createVideoResult(final Uri fileUri) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                Uri thumbnailUri = createVideoThumbnail(fileUri);
                try {
                    JSONObject response = getMediaDictionary(fileUri, thumbnailUri);
                    mCallbackContext.success(response);
                } catch (Exception ex) {
                    Log.e(TAG, "failed to send response", ex);
                    mCallbackContext.error(ex.getMessage());
                }
            }
        });
    }

    private Uri createVideoThumbnail(Uri fileUri) {
        String filePath = fileUri.getPath();
        String thumbnailPath = FileHelper.removeFileExtensionFromPath(filePath) + ".png";

        Bitmap thumb = ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Images.Thumbnails.MINI_KIND);

        try {
            FileOutputStream out = new FileOutputStream(thumbnailPath);
            thumb.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return Uri.fromFile(new File(thumbnailPath));
    }

    private double getVideoLength(Uri fileUri) {
        MediaMetadataRetriever retriever = null;
        try {
            retriever = new MediaMetadataRetriever();
            //use one of overloaded setDataSource() functions to set your data source
            retriever.setDataSource(cordova.getActivity(), fileUri);
            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            return Double.parseDouble(time) / 1000.0;
        } catch (Exception e) {
            Log.e(TAG, "failed to get video length", e);
            return 0;
        } finally {
            if (retriever != null) {
                retriever.release();
            }
        }
    }

    private JSONObject getMediaDictionary(Uri fileUri, Uri thumbnailUri) throws Exception {
        File fp = webView.getResourceApi().mapUriToFile(fileUri);
        File tfp = webView.getResourceApi().mapUriToFile(thumbnailUri);

        PluginManager pm = getPluginManager();
        if (pm == null) {
            throw new Exception("Failed to get plugin manager instance");
        }

        FileUtils filePlugin = (FileUtils) pm.getPlugin("File");
        LocalFilesystemURL url = filePlugin.filesystemURLforLocalPath(fp.getAbsolutePath());
        LocalFilesystemURL turl = filePlugin.filesystemURLforLocalPath(tfp.getAbsolutePath());

        double videoLength = getVideoLength(fileUri);

        JSONObject response = new JSONObject();
        response.put("name", fp.getName());
        response.put("fullPath", Uri.fromFile(fp));
        if (url != null) {
            response.put("localURL", url.toString());
        }

        response.put("thumbnailName", tfp.getName());
        response.put("fullThumbnailPath", Uri.fromFile(tfp));
        if (turl != null) {
            response.put("localThumbnailURL", turl.toString());
        }

        response.put("type", FileHelper.getMimeType(Uri.fromFile(fp), cordova));
        response.put("size", fp.length());
        response.put("lastModifiedDate", fp.lastModified());
        response.put("duration", videoLength);
        return response;
    }

    @SuppressWarnings("unchecked")
    private PluginManager getPluginManager() {
        Class webViewClass = webView.getClass();
        PluginManager pm = null;
        try {
            Method gpm = webViewClass.getMethod("getPluginManager");
            pm = (PluginManager) gpm.invoke(webView);
        } catch (Exception e) {
            //eat
        }

        if (pm == null) {
            try {
                Field pmf = webViewClass.getField("pluginManager");
                pm = (PluginManager) pmf.get(webView);
            } catch (Exception e) {
                //eat
            }
        }
        return pm;
    }
}