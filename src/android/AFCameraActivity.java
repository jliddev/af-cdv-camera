package biz.appform.camera;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Player.EventListener;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraView;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

public class AFCameraActivity extends Activity implements EventListener {
    private static final String TAG = AFCameraActivity.class.getSimpleName();
    private static final int DEFAULT_DURATION = 60000; //1 minute in ms
    private static final int MODE_RECORD_READY = 0x01;
    private static final int MODE_RECORD_ACTIVE = 0x02;
    private static final int MODE_PREVIEW = 0x03;

    private CameraView mCameraView;
    private View mPlayerContainer;
    private View mCameraContainer;
    private View mRecordButton;
    private View mStopButton;
    private TextView mCountdownLabel;

    private int mDuration = DEFAULT_DURATION;
    private int mRemainingSeconds = 0;
    private String mFilePath = null;
    private File mFile = null;
    private File mLastRecordedFile = null;
    private Timer mCountdownTimer = null;
    private int mMode = MODE_RECORD_READY;

    //playback vars
    private static final DefaultBandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter();

    private Button mCancelButton;
    private Button mUseVideoButton;
    private Handler mainHandler;
    private SimpleExoPlayerView simpleExoPlayerView;
    private SimpleExoPlayer player;

    private DefaultTrackSelector trackSelector;
    private DataSource.Factory mediaDataSourceFactory;

    private boolean inErrorState;
    private int resumeWindow;
    private long resumePosition;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        clearResumePosition();
        setContentView(getLayoutIdentifier("afcamera_activity"));

        mainHandler = new Handler();

        mediaDataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "AFVideoPlayer"), BANDWIDTH_METER);

        Intent intent = getIntent();
        if (intent.hasExtra("duration")) {
            mDuration = intent.getIntExtra("duration", DEFAULT_DURATION);
        }

        if( intent.hasExtra("filepath") ){
            mFile = new File(intent.getStringExtra("filepath"));
        }
        else {
            Log.e(TAG, "filepath intent extra not found");
            finish();
            return;
        }

        mCameraContainer = findViewById(getIdentifier("camera_container"));
        mPlayerContainer = findViewById(getIdentifier("player_container"));
        mCameraView = (CameraView) findViewById(getIdentifier("camera"));
        mRecordButton = findViewById(getIdentifier("record_btn"));
        mCancelButton = (Button) findViewById(getIdentifier("cancel_btn"));
        mUseVideoButton = (Button) findViewById(getIdentifier("use_video_btn"));
        mStopButton = findViewById(getIdentifier("stop_btn"));
        mCountdownLabel = (TextView) findViewById(getIdentifier("countdown_lbl"));

        simpleExoPlayerView = (SimpleExoPlayerView) findViewById(getIdentifier("player_view"));
//        simpleExoPlayerView.requestFocus();

        mRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleClickRecord();
            }
        });

        mStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleClickStop();
            }
        });

        mCameraView.addCameraListener(new CameraListener() {
            @Override
            public void onVideoTaken(File video) {
                // The File is the same you passed before.
                // Now it holds a MP4 video.
                handleVideoRecorded(video);
            }
        });

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleClickCancel();
            }
        });

        mUseVideoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleClickUseVideo();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCameraView.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCameraView.stop();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCameraView.destroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            initializePlayer();
        } else {
            showToast(getResourceString("storage_permission_denied"));
            finish();
        }
    }

    private void toggleRecordButtons() {
        toggleButtonVisibility(mRecordButton);
        toggleButtonVisibility(mStopButton);
    }

    private void toggleButtonVisibility(View button) {
        if (button.getVisibility() == View.VISIBLE) {
            button.setVisibility(View.GONE);
        } else {
            button.setVisibility(View.VISIBLE);
        }
    }

    private void handleClickRecord() {
        if (mMode != MODE_RECORD_READY) {
            return;
        }

        toggleRecordButtons();
        mCameraView.startCapturingVideo(mFile, mDuration);
        startCountdownTimer();
        mCountdownLabel.setVisibility(View.VISIBLE);
        mMode = MODE_RECORD_ACTIVE;
    }

    private void handleClickStop() {
        if (mMode != MODE_RECORD_ACTIVE) {
            return;
        }

        toggleRecordButtons();
        mCameraView.stopCapturingVideo();
    }

    private void handleClickCancel() {
        if (mMode != MODE_PREVIEW) {
            return;
        }

        finish();
//
//        mMode = MODE_RECORD_READY;
//
//        mCameraView.start();
//        mCameraContainer.setVisibility(View.VISIBLE);
//        mPlayerContainer.setVisibility(View.GONE);
    }

    private void handleClickUseVideo() {
        if (mMode != MODE_PREVIEW) {
            return;
        }

        Intent result = new Intent();
        result.setData(Uri.fromFile(mLastRecordedFile));
        this.setResult(RESULT_OK, result);

        finish();
    }

    private void startCountdownTimer() {
        mRemainingSeconds = mDuration / 1000;

        updateCountdown(mRemainingSeconds);

        mCountdownTimer = new Timer();
        mCountdownTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                mRemainingSeconds -= 1;
                updateCountdown(mRemainingSeconds);
            }
        }, 1000, 1000);
    }

    private void updateCountdown(final int seconds) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int mins = seconds / 60;
                int sec = seconds % 60;

                String time = String.format(Locale.getDefault(), "%d:%02d", mins, sec);

                mCountdownLabel.setText(time);
            }
        });
    }

    private void handleVideoRecorded(File video) {
        mMode = MODE_PREVIEW;

        mCountdownTimer.cancel();
        mCountdownTimer = null;

        mLastRecordedFile = video;

        mCameraView.stop();
        mCountdownLabel.setVisibility(View.GONE);
        mCameraContainer.setVisibility(View.GONE);
        mPlayerContainer.setVisibility(View.VISIBLE);

        simpleExoPlayerView.requestFocus();
        initializePlayer();
    }

    ///VIDEO METHODS
    private void initializePlayer() {
        Intent intent = getIntent();
        boolean needNewPlayer = player == null;
        if (needNewPlayer) {
            TrackSelection.Factory adaptiveTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(BANDWIDTH_METER);
            trackSelector = new DefaultTrackSelector(adaptiveTrackSelectionFactory);

            player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
            player.addListener(this);

            simpleExoPlayerView.setPlayer(player);
            player.setPlayWhenReady(true);
        }

        if (mLastRecordedFile == null) {
            return;
        }

        Uri uri = Uri.fromFile(mLastRecordedFile);

        if (Util.maybeRequestReadExternalStoragePermission(this, uri)) {
            // The player will be reinitialized if the permission is granted.
            return;
        }

        MediaSource ms;
        int mediaType = Util.inferContentType(uri);
        switch (mediaType) {
            case C.TYPE_OTHER:
                ms = new ExtractorMediaSource(
                        uri,
                        mediaDataSourceFactory,
                        new DefaultExtractorsFactory(),
                        mainHandler,
                        null
                );
                break;
            default: {
                throw new IllegalStateException("Unsupported type: " + mediaType);
            }
        }

        boolean haveResumePosition = resumeWindow != C.INDEX_UNSET;
        if (haveResumePosition) {
            player.seekTo(resumeWindow, resumePosition);
        }

        player.prepare(ms, !haveResumePosition, false);
        inErrorState = false;
    }

    private void clearResumePosition() {
        resumeWindow = C.INDEX_UNSET;
        resumePosition = C.TIME_UNSET;
    }

    private void updateResumePosition() {
        resumeWindow = player.getCurrentWindowIndex();
        resumePosition = Math.max(0, player.getContentPosition());
    }

    private void releasePlayer() {
        if (player != null) {
            updateResumePosition();
            player.release();
            player = null;
        }
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    private static boolean isBehindLiveWindow(ExoPlaybackException e) {
        if (e.type != ExoPlaybackException.TYPE_SOURCE) {
            return false;
        }
        Throwable cause = e.getSourceException();
        while (cause != null) {
            if (cause instanceof BehindLiveWindowException) {
                return true;
            }
            cause = cause.getCause();
        }
        return false;
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {
        // Do nothing.
    }

    @Override
    public void onPositionDiscontinuity() {
        if (inErrorState) {
            // This will only occur if the user has performed a seek whilst in the error state. Update the
            // resume position so that if the user then retries, playback will resume from the position to
            // which they seeked.
            updateResumePosition();
        }
    }

    @Override
    public void onPlayerError(ExoPlaybackException e) {
        String errorString = null;
        if (e.type == ExoPlaybackException.TYPE_RENDERER) {
            Exception cause = e.getRendererException();
            if (cause instanceof MediaCodecRenderer.DecoderInitializationException) {
                // Special case for decoder initialization failures.
                MediaCodecRenderer.DecoderInitializationException decoderInitializationException =
                        (MediaCodecRenderer.DecoderInitializationException) cause;
                if (decoderInitializationException.decoderName == null) {
                    if (decoderInitializationException.getCause() instanceof MediaCodecUtil.DecoderQueryException) {
                        errorString = getString(getStringIdentifier("error_querying_decoders"));
                    } else if (decoderInitializationException.secureDecoderRequired) {
                        errorString = getString(getStringIdentifier("error_no_secure_decoder"),
                                decoderInitializationException.mimeType);
                    } else {
                        errorString = getString(getStringIdentifier("error_no_decoder"),
                                decoderInitializationException.mimeType);
                    }
                } else {
                    errorString = getString(getStringIdentifier("error_instantiating_decoder"),
                            decoderInitializationException.decoderName);
                }
            }
        }
        if (errorString != null) {
            showToast(errorString);
        }
        inErrorState = true;
        if (isBehindLiveWindow(e)) {
            clearResumePosition();
            initializePlayer();
        } else {
            updateResumePosition();
        }
    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
        //empty
    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
        // Do nothing.
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
        // Do nothing.
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {
        // Do nothing.
    }

    /**
     * We have to do this since we cant include a reference to R like a normal app
     *
     * @param name
     * @return
     */
    private String getResourceString(String name) {
        return getString(getStringIdentifier(name));
    }

    private int getStringIdentifier(String name) {
        return getResourceIdentifier(name, "string");
    }

    private int getLayoutIdentifier(String name) {
        return getResourceIdentifier(name, "layout");
    }

    private int getIdentifier(String name) {
        return getResourceIdentifier(name, "id");
    }

    private int getResourceIdentifier(String name, String type) {
        return getResources().getIdentifier(name, type, getPackageName());
    }
}