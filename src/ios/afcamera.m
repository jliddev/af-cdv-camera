#import "AfCamera.h"
#import "CDVFile.h"
#import <Cordova/CDVAvailability.h>

@implementation NSBundle (PluginExtensions)

+ (NSBundle*) pluginBundle:(CDVPlugin*)plugin {
    NSBundle* bundle = [NSBundle bundleWithPath: [[NSBundle mainBundle] pathForResource:NSStringFromClass([plugin class]) ofType: @"bundle"]];
    return bundle;
}
@end

#define PluginLocalizedString(plugin, key, comment) [[NSBundle pluginBundle:(plugin)] localizedStringForKey:(key) value:nil table:nil]

@implementation CDVImagePicker

@synthesize quality;
@synthesize callbackId;
@synthesize mimeType;
@synthesize outputDir;

- (uint64_t)accessibilityTraits
{
    NSString* systemVersion = [[UIDevice currentDevice] systemVersion];
    
    if (([systemVersion compare:@"4.0" options:NSNumericSearch] != NSOrderedAscending)) { // this means system version is not less than 4.0
        return UIAccessibilityTraitStartsMediaSession;
    }
    
    return UIAccessibilityTraitNone;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (UIViewController*)childViewControllerForStatusBarHidden {
    return nil;
}

- (void)viewWillAppear:(BOOL)animated {
    SEL sel = NSSelectorFromString(@"setNeedsStatusBarAppearanceUpdate");
    if ([self respondsToSelector:sel]) {
        [self performSelector:sel withObject:nil afterDelay:0];
    }
    
    [super viewWillAppear:animated];
}

@end

@implementation AfCamera
@synthesize inUse;

- (void)pluginInitialize
{
    self.inUse = NO;
}

- (void) recordVideo:(CDVInvokedUrlCommand *)command
{
    NSString* callbackId = command.callbackId;
    NSDictionary* options = [command argumentAtIndex:0];
    NSString* outputDir = [options objectForKey:@"outputDir"];
    
    if ([options isKindOfClass:[NSNull class]]) {
        options = [NSDictionary dictionary];
    }
    
    NSNumber* duration = [options objectForKey:@"duration"];
    NSString* mediaType = nil;
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        // there is a camera, it is available, make sure it can do movies
        pickerController = [[CDVImagePicker alloc] init];
        
        NSArray* types = nil;
        if ([UIImagePickerController respondsToSelector:@selector(availableMediaTypesForSourceType:)]) {
            types = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
            // NSLog(@"MediaTypes: %@", [types description]);
            
            if ([types containsObject:(NSString*)kUTTypeMovie]) {
                mediaType = (NSString*)kUTTypeMovie;
            } else if ([types containsObject:(NSString*)kUTTypeVideo]) {
                mediaType = (NSString*)kUTTypeVideo;
            }
        }
    }
    
    if (!mediaType) {
        // don't have video camera return error
        NSLog(@"Capture.captureVideo: video mode not available.");
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageToErrorObject:CAPTURE_NOT_SUPPORTED];
        [self.commandDelegate sendPluginResult:result callbackId:callbackId];
        pickerController = nil;
    } else {
        [self showAlertIfAccessProhibited];
        
        pickerController.delegate = self;
        pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        pickerController.allowsEditing = NO;
        // updated to 1280 x 720
        pickerController.videoQuality = UIImagePickerControllerQualityTypeIFrame1280x720;
        // iOS 3.0
        pickerController.mediaTypes = [NSArray arrayWithObjects:mediaType, nil];
        
        if ([mediaType isEqualToString:(NSString*)kUTTypeMovie]){
            if (duration) {
                pickerController.videoMaximumDuration = [duration doubleValue];
            }
            //NSLog(@"pickerController.videoMaximumDuration = %f", pickerController.videoMaximumDuration);
        }
        
        // iOS 4.0
        if ([pickerController respondsToSelector:@selector(cameraCaptureMode)]) {
            pickerController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModeVideo;
            
            // pickerController.cameraDevice = UIImagePickerControllerCameraDeviceRear;
            // pickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
        }
        // CDVImagePicker specific property
        pickerController.callbackId = callbackId;
        pickerController.outputDir = outputDir;
        
        [self.viewController presentViewController:pickerController animated:YES completion:nil];
    }
}

- (void)showAlertIfAccessProhibited
{
    if (![self hasCameraAccess]) {
        [self showPermissionsAlert];
    }
}

- (BOOL)hasCameraAccess
{
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    return status != AVAuthorizationStatusDenied && status != AVAuthorizationStatusRestricted;
}

- (void)showPermissionsAlert
{
    __weak AfCamera* weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[UIAlertView alloc] initWithTitle:[[NSBundle mainBundle]
                                             objectForInfoDictionaryKey:@"CFBundleDisplayName"]
                                    message:NSLocalizedString(@"Access to the camera has been prohibited; please enable it in the Settings app to continue.", nil)
                                   delegate:weakSelf
                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                          otherButtonTitles:NSLocalizedString(@"Settings", nil), nil] show];
    });
}

- (void)processVideo:(NSString*)moviePath forCallbackId:(NSString*)callbackId
{
    NSString* thumbnailPath = [self createVideoThumbnail:moviePath];
    
    // create MediaFile object
    NSDictionary* fileDict = [self getMediaDictionaryFromPath:moviePath ofType:nil withThumbnail:thumbnailPath];
    //NSArray* fileArray = [NSArray arrayWithObject:fileDict];
    
    CDVPluginResult* result = nil;
    result = [CDVPluginResult  resultWithStatus:CDVCommandStatus_OK messageAsDictionary:fileDict];
    
    [self.commandDelegate sendPluginResult:result callbackId:callbackId];
}

- (NSString*)getOutputDirPath:(NSString*)outputDir
{
    // Get canonical version of localPath
    NSURL *fileURL = [NSURL URLWithString:outputDir];
    NSURL *resolvedFileURL = [fileURL URLByResolvingSymlinksInPath];
    NSString *path = [resolvedFileURL path];
    
    return path;
}

- (NSDictionary*)getMediaDictionaryFromPath:(NSString*)fullPath ofType:(NSString*)type withThumbnail:(NSString*)thumbPath
{
    NSFileManager* fileMgr = [[NSFileManager alloc] init];
    NSMutableDictionary* fileDict = [NSMutableDictionary dictionaryWithCapacity:5];
    
    CDVFile *fs = [self.commandDelegate getCommandInstance:@"File"];
    
    // Get canonical version of localPath
    NSURL *fileURL = [NSURL URLWithString:[NSString stringWithFormat:@"file://%@", fullPath]];
    NSURL *resolvedFileURL = [fileURL URLByResolvingSymlinksInPath];
    NSString *path = [resolvedFileURL path];
    
    CDVFilesystemURL *url = [fs fileSystemURLforLocalPath:path];
    
    [fileDict setObject:[fullPath lastPathComponent] forKey:@"name"];
    [fileDict setObject:fullPath forKey:@"fullPath"];
    if (url) {
        [fileDict setObject:[url absoluteURL] forKey:@"localURL"];
    }
    
    //get the thumbnail paths
    if( thumbPath != nil )
    {
        NSURL *thumbUrl = [NSURL URLWithString:[NSString stringWithFormat:@"file://%@", thumbPath]];
        NSURL *resolvedThumbURL = [thumbUrl URLByResolvingSymlinksInPath];
        NSString *tpath = [resolvedThumbURL path];
        
        CDVFilesystemURL *url = [fs fileSystemURLforLocalPath:tpath];
        
        [fileDict setObject:[thumbPath lastPathComponent] forKey:@"thumbnailName"];
        [fileDict setObject:thumbPath forKey:@"fullThumbnailPath"];
        if( tpath )
        {
            [fileDict setObject:[url absoluteURL] forKey:@"localThumbnailURL"];
        }
    }
    
    // determine type
    if (!type) {
        id command = [self.commandDelegate getCommandInstance:@"File"];
        if ([command isKindOfClass:[CDVFile class]]) {
            CDVFile* cdvFile = (CDVFile*)command;
            NSString* mimeType = [cdvFile getMimeTypeFromPath:fullPath];
            [fileDict setObject:(mimeType != nil ? (NSObject*)mimeType : [NSNull null]) forKey:@"type"];
        }
    }
    NSDictionary* fileAttrs = [fileMgr attributesOfItemAtPath:fullPath error:nil];
    [fileDict setObject:[NSNumber numberWithUnsignedLongLong:[fileAttrs fileSize]] forKey:@"size"];
    NSDate* modDate = [fileAttrs fileModificationDate];
    NSNumber* msDate = [NSNumber numberWithDouble:[modDate timeIntervalSince1970] * 1000];
    [fileDict setObject:msDate forKey:@"lastModifiedDate"];
    [fileDict setValue:[self getVideoLength:fileURL] forKey:@"duration"];
    return fileDict;
}

#pragma mark Video Conversion

- (NSString*)createVideoThumbnail:(NSString*)videoPath
{
    //generate the path to the thumbnail withe same name different extension
    NSString* thumbnailPath = [[videoPath stringByDeletingPathExtension] stringByAppendingPathExtension:@"png"];
    
    NSURL* videoUrl = [[NSURL alloc] initFileURLWithPath:videoPath];
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoUrl options:nil];
    AVAssetImageGenerator *generateImg = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    
    NSError *error = nil;
    CMTime time = CMTimeMake(1, 65);
    CGImageRef refImg = [generateImg copyCGImageAtTime:time actualTime:NULL error:&error];
    //NSLog(@"error==%@, Refimage==%@", error, refImg);
    
    UIImage *FrameImage= [[UIImage alloc] initWithCGImage:refImg];
    
    //convert the image we made to a png
    NSData *data = UIImagePNGRepresentation(FrameImage);
    
    //save the png data to a file
    if( ![data writeToFile:thumbnailPath atomically:YES] )
    {
        NSLog(@"failed to write thumbnail data");
        return nil;
    }
    
    return thumbnailPath;
}

- (void)convertVideoToMpeg:(NSString*)videoPath withCallbackId:(NSString*)callbackId andOutputDir:(NSString*)outputDir {
    // MP4 Conversion using the AVFoundation Framework
    
    [self.commandDelegate runInBackground:^{
        
        // Create the asset url with the video file
        
        NSURL *videoURL = [[NSURL alloc] initFileURLWithPath:videoPath];
        
        AVURLAsset *avAsset = [AVURLAsset URLAssetWithURL:videoURL options:nil];
        NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:avAsset];
        
        // Check if video is supported for conversion or not
        if ([compatiblePresets containsObject:AVAssetExportPresetMediumQuality]) {
            // Create Export session
            AVAssetExportSession *exportSession = [[AVAssetExportSession alloc]initWithAsset:avAsset presetName:AVAssetExportPresetMediumQuality];
            
            // Creating temp path to save the converted video
            
            // NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            // use temp directory
            
            NSString* tempDirectory = NSTemporaryDirectory();
            
            NSString* myDocumentPath;
            if( outputDir != nil )
            {
                NSString* cleanOutputDir = [self getOutputDirPath:outputDir];
                NSString* existingFileName = [[videoPath lastPathComponent] stringByDeletingPathExtension];
                myDocumentPath = [[cleanOutputDir stringByAppendingPathComponent:existingFileName] stringByAppendingPathExtension:@"mp4"];
                
                BOOL isDir = NO;
                NSError* err;
                if( ![[NSFileManager defaultManager] fileExistsAtPath:cleanOutputDir isDirectory:&isDir] )
                {
                    if( ![[NSFileManager defaultManager] createDirectoryAtPath:cleanOutputDir withIntermediateDirectories:YES attributes:nil error:&err] )
                    {
                        //send an error if we couldnt make the container folders for our new video
                        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[@"Failed to create outputDir: " stringByAppendingString:[err description]]];
                        
                        [self.commandDelegate sendPluginResult:result callbackId:callbackId];
                        
                        return;
                    }
                }
            }
            else
            {
                myDocumentPath = [tempDirectory stringByAppendingPathComponent:@"capturedVideo.mp4"];
            }
            
            NSURL *url = [[NSURL alloc] initFileURLWithPath:myDocumentPath];
            
            // Check if the file already exists then remove the previous file
            if ([[NSFileManager defaultManager]fileExistsAtPath:myDocumentPath]) {
                [[NSFileManager defaultManager]removeItemAtPath:myDocumentPath error:nil];
            }
            
            NSLog(@"Creating export file: %@",myDocumentPath);
            
            exportSession.outputURL = url;
            
            // Set the output file format, etc.
            exportSession.outputFileType = AVFileTypeMPEG4;
            exportSession.shouldOptimizeForNetworkUse = YES;
            
            [exportSession exportAsynchronouslyWithCompletionHandler:^{
                
                if( [[NSFileManager defaultManager] fileExistsAtPath:videoPath] )
                {
                    if( ![[NSFileManager defaultManager] removeItemAtPath:videoPath error:nil] )
                    {
                        NSLog(@"failed to delete temp video");
                    }
                }
                
                switch ([exportSession status])    {
                    case AVAssetExportSessionStatusFailed:
                        NSLog(@"Video export session failed");
                        [self conversionFailedOrCanceled:callbackId];
                        break;
                    case AVAssetExportSessionStatusCancelled:
                        NSLog(@"Video export canceled");
                        [self conversionFailedOrCanceled:callbackId];
                        break;
                    case AVAssetExportSessionStatusCompleted:
                        //Video conversion finished
                        NSLog(@"Video MPEG compression export successful!");
                        [self processVideo:myDocumentPath forCallbackId:callbackId];
                        break;
                    default:
                        break;
                }
            }];
        }
        else {
            NSLog(@"Video file not supported!");
        }
    }];
}

- (void)conversionFailedOrCanceled:(NSString*)callbackId {
    CDVPluginResult* result = nil;
    result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageToErrorObject:CAPTURE_INTERNAL_ERR];
    [self.commandDelegate sendPluginResult:result callbackId:callbackId];
};


#pragma mark CALLBACKS

- (void)imagePickerController:(UIImagePickerController*)picker didFinishPickingImage:(UIImage*)image editingInfo:(NSDictionary*)editingInfo
{
    // older api calls new one
    [self imagePickerController:picker didFinishPickingMediaWithInfo:editingInfo];
}

- (void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    CDVImagePicker* cameraPicker = (CDVImagePicker*)picker;
    NSString* callbackId = cameraPicker.callbackId;
    NSString* outputDir = cameraPicker.outputDir;
    
    [[picker presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    
    NSString* mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString*)kUTTypeMovie]) {
        // process video
        NSString* moviePath = [(NSURL *)[info objectForKey:UIImagePickerControllerMediaURL] path];
        if (moviePath) {
            
            [self convertVideoToMpeg:moviePath withCallbackId:callbackId andOutputDir:outputDir];
            //result = [self processVideo:moviePath forCallbackId:callbackId andOutputDir:cameraPicker.outputDir];
        }
    }
    else
    {
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageToErrorObject:CAPTURE_INTERNAL_ERR];
        [self.commandDelegate sendPluginResult:result callbackId:callbackId];
    }
    
    pickerController = nil;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController*)picker
{
    CDVImagePicker* cameraPicker = (CDVImagePicker*)picker;
    NSString* callbackId = cameraPicker.callbackId;
    
    [[picker presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    
    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageToErrorObject:CAPTURE_NO_MEDIA_FILES];
    [self.commandDelegate sendPluginResult:result callbackId:callbackId];
    pickerController = nil;
}

- (NSNumber*)getVideoLength:(NSURL*)sourceMovieURL
{
    AVURLAsset *sourceAsset = [AVURLAsset URLAssetWithURL:sourceMovieURL options:nil];
    CMTime duration = sourceAsset.duration;
    return [NSNumber numberWithFloat: CMTimeGetSeconds(duration)];
}

@end

