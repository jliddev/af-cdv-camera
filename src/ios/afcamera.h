#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import "CDVFile.h"

enum CDVCaptureError {
    CAPTURE_INTERNAL_ERR = 0,
    CAPTURE_APPLICATION_BUSY = 1,
    CAPTURE_INVALID_ARGUMENT = 2,
    CAPTURE_NO_MEDIA_FILES = 3,
    CAPTURE_PERMISSION_DENIED = 4,
    CAPTURE_NOT_SUPPORTED = 20
};
typedef NSUInteger CDVCaptureError;

@interface CDVImagePicker : UIImagePickerController
{
    NSString* callbackid;
    NSInteger quality;
    NSString* mimeType;
    NSString* outputDir;
}
@property (assign) NSInteger quality;
@property (copy)   NSString* callbackId;
@property (copy)   NSString* mimeType;
@property (copy) NSString* outputDir;

@end

@interface AfCamera : CDVPlugin <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    CDVImagePicker* pickerController;
    BOOL inUse;
}
@property BOOL inUse;

-(void)recordVideo:(CDVInvokedUrlCommand*)command;
- (float)getVideoLength;
@end

