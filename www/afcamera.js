var cordova = require('cordova');

var afcamera = {
	recordVideo: function(options, successFunction, errorFunction) {
		cordova.exec(successFunction, errorFunction, "afcamera", "recordVideo", [options]);
	}
};

module.exports = afcamera;