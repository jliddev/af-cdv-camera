af-cdv-camera
install the plugman CLI:

$ npm install -g plugman
create a package.json file for your plugin:

$ plugman createpackagejson /path/to/your/plugin
publish it:

$ npm adduser # that is if you don't have an account yet
$ npm publish .
